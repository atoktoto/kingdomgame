package atk.twitchgame;


import atk.twitchgame.game.events.Event;
import atk.twitchgame.game.events.MovePawnEvent;
import atk.twitchgame.irc.ChatMessage;
import atk.twitchgame.irc.TwitchChatBot;
import org.jibble.pircbot.IrcException;
import rx.Subscriber;
import rx.schedulers.Schedulers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.*;

public class TextController {

    final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    final TwitchChatBot chatBot = new TwitchChatBot();
    final Deque<Event> eventQueue = new ConcurrentLinkedDeque<>();

    public void connect() {
        executor.submit(() -> {
            try {
                chatBot.connect("jubii2.dk.quakenet.org", 6667);
                chatBot.changeNick("gameBot");
                chatBot.joinChannel("#atok_test");
                chatBot.getObservable()
                        .observeOn(Schedulers.from(executor))
                        .subscribe(new Subscriber<ChatMessage>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(ChatMessage chatMessage) {
                                parseMessage(chatMessage);
                            }
                        });
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IrcException e) {
                e.printStackTrace();
            }
        });
    }

    private void parseMessage(ChatMessage chatMessage) {
        String[] split = chatMessage.message.split(" ");

        if(chatMessage.message.startsWith("move")) {
            if(split.length > 0) {
                int direction = Integer.parseInt(split[1]);
                eventQueue.add(new MovePawnEvent(direction, chatMessage.sender));
            }
        }
    }

    public List<Event> getEvents() {
        ArrayList<Event> list = new ArrayList<>();
        Event event;
        while((event = eventQueue.poll()) != null) {
            list.add(event);
        }
        return list;
    }
}
