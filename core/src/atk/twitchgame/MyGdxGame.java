package atk.twitchgame;

import atk.twitchgame.game.GameWorld;
import atk.twitchgame.game.gameobjects.PawnFactory;
import atk.twitchgame.game.events.EnterEvent;
import atk.twitchgame.game.events.Event;
import atk.twitchgame.gfx.AnimatedTilesParser;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.maps.tiled.*;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.List;

public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;

	private TiledMap map;
	private BatchTiledMapRenderer renderer;
	private OrthographicCamera camera;
	private BitmapFont font;

	float stateTime = 0;
	private Stage stage;

	private TextureAtlas atlas;
	private GameWorld gameWorld;

	private TextController textController = new TextController();

	@Override
	public void create () {
		batch = new SpriteBatch();
		font = new BitmapFont();

		this.map = new TmxMapLoader().load("examplemap1.tmx");
		AnimatedTilesParser.makeAnimated(map);

		this.renderer = new OrthogonalTiledMapRenderer(map, 1);

		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
		camera.update();

		atlas = new TextureAtlas(Gdx.files.internal("packed/base.atlas"));

		stage = new Stage(new ScreenViewport());
		gameWorld = new GameWorld(stage, new PawnFactory(atlas, font), map);
		gameWorld.createPawn(new EnterEvent("atok", "aladdin"));

		textController.connect();
	}



	@Override
	public void render () {
		stateTime += Gdx.graphics.getDeltaTime();

		List<Event> events = textController.getEvents();
		for (Event event : events) {
			Gdx.app.log("Event", event.toString());
			gameWorld.executeEvent(event);
		}

		Gdx.gl.glClearColor(0.55f, 0.55f, 0.55f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		renderer.setView(camera);
		renderer.render();

		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();

		batch.begin();
		font.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 10, 20);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		stage.getViewport().update(width, height, true);
	}
}
