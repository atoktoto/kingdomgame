package atk.twitchgame.irc;

import org.jibble.pircbot.PircBot;
import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;


public class TwitchChatBot extends PircBot  {

    final Subject<ChatMessage, ChatMessage> subject =  new SerializedSubject<>(PublishSubject.create());
    public Observable<ChatMessage> getObservable() {
        return subject;
    }

    @Override
    protected void onConnect() {
        super.onConnect();
    }

    @Override
    protected void onMessage(String channel, String sender, String login, String hostname, String message) {
        subject.onNext(new ChatMessage(sender, message));
    }
}
