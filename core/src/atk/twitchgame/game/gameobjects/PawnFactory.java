package atk.twitchgame.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class PawnFactory {

    private final TextureAtlas atlas;
    private final BitmapFont font;

    public PawnFactory(TextureAtlas atlas, BitmapFont font) {
        this.atlas = atlas;
        this.font = font;
    }

    private Animation[] createAnimationsFromAtlas(String style) {
        TextureAtlas.AtlasRegion region = atlas.findRegion("pawns/" + style);
        TextureRegion[][] regions = region.split(32, 48);
        Animation[] animations = new Animation[4];
        for(int y = 0; y < 4; y++) {
            animations[y] = new Animation(0.2f, regions[y]);
        }
        return animations;
    }

    public Pawn createPawn(String style, String label) {
        Animation[] animations = createAnimationsFromAtlas(style);
        PawnActor pawnActor = new PawnActor(animations, label, font);
        pawnActor.setScale(0.5f);
        Pawn pawn = new Pawn(pawnActor);
        return pawn;
    }
}
