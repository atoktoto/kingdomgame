package atk.twitchgame.game.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.action;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;


public class PawnActor extends Actor {

    private final Animation[] animations;
    private final BitmapFont font;

    private int faceDirection = 0;
    private boolean walking = false;

    private float time = 0;
    private String label;

    public PawnActor(Animation[] animations, String label, BitmapFont font) {
        super();
        this.label = label;
        this.animations = animations;
        this.font = font;

        TextureRegion exampleRegion = animations[0].getKeyFrames()[0];
        setWidth(exampleRegion.getRegionWidth());
        setHeight(exampleRegion.getRegionHeight());
    }

    public void setFaceDirection(int faceDirection) {
        this.faceDirection = faceDirection;
    }

    public void setWalking(boolean walking) {
        this.walking = walking;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        time += delta;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        TextureRegion region;
        if(faceDirection == 0) {
            region = animations[0].getKeyFrames()[0];
        } else {
            int animNumber = faceDirection - 1;
            if(walking) {
                region = animations[animNumber].getKeyFrame(time, true);
            } else {
                region = animations[animNumber].getKeyFrames()[0];
            }
        }
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(region, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());

        font.draw(batch, label, getX() - font.getBounds(label).width / 2 + getWidth() * getScaleX() / 2, getY() + getHeight() * getScaleY() + font.getXHeight() + 5);
    }

    /**
     * Stops previous actions and starts to move in direction.
     *
     * @param direction
     * @param tileSize
     * @return
     */
    public boolean move(int direction, int tileSize) {
        clearActions();

        SequenceAction sequence = action(SequenceAction.class);
        int lastDir = faceDirection;

        if(lastDir != direction) {
            sequence.addAction(Actions.run(() -> setWalking(false)));
            sequence.addAction(delay(0.3f));
        }
        sequence.addAction(Actions.run(() -> setWalking(true)));
        sequence.addAction(Actions.run(() -> setFaceDirection(direction)));
        sequence.addAction(directionToMoveByAction(direction, tileSize));

        sequence.addAction(Actions.run(() -> setFaceDirection(0)));
        addAction(sequence);
        return true;
    }

    public MoveByAction directionToMoveByAction(int direction, int tileSize) {
        MoveByAction action = Actions.action(MoveByAction.class);
        action.setAmountX(0);
        action.setAmountY(0);
        action.setDuration(1);
        if(direction == 1) {
            action.setAmountY(-tileSize);
        } else if(direction == 2) {
            action.setAmountX(-tileSize);
        } else if(direction == 3) {
            action.setAmountX(tileSize);
        } else if(direction == 4) {
            action.setAmountY(tileSize);
        } else {
            throw new RuntimeException("WTF?");
        }
        return action;
    }

}
