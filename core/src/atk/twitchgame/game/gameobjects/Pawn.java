package atk.twitchgame.game.gameobjects;

public class Pawn extends GameObject {

    int position_x;
    int position_y;

    public Pawn(PawnActor actor) {
        super(actor);
    }

    @Override
    public PawnActor getActor() {
        return (PawnActor)super.getActor();
    }

    public void move(int direction) {
        int tileSize = 32;
        getActor().move(direction, tileSize);
    }

    public void moveTo(int x, int y) {
        position_x = x;
        position_y = y;


    }

    @Override
    public void tick() {


    }
}
