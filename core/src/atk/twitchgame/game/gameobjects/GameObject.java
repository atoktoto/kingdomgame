package atk.twitchgame.game.gameobjects;


import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class GameObject {
    private final Actor actor;

    public GameObject(Actor actor) {
        this.actor = actor;
    }

    public Actor getActor() {
        return actor;
    }

    public void tick() {

    }
}
