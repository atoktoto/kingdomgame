package atk.twitchgame.game.events;


public class Event {
    public final String playerNickname;

    public Event(String playerNickname) {
        this.playerNickname = playerNickname;
    }
}
