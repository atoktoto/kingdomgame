package atk.twitchgame.game.events;


public class MovePawnEvent extends Event {
    public final int direction;

    public MovePawnEvent(int direction, String playerNickname) {
        super(playerNickname);
        this.direction = direction;
    }
}
