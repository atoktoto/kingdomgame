package atk.twitchgame.game.events;


public class EnterEvent extends Event {
    public final String playerNickname;
    public final String pawnStyle;

    public EnterEvent(String playerNickname, String pawnStyle) {
        super(playerNickname);
        this.playerNickname = playerNickname;
        this.pawnStyle = pawnStyle;
    }
}
