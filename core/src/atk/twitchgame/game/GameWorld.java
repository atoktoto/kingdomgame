package atk.twitchgame.game;


import atk.twitchgame.game.gameobjects.PawnFactory;
import atk.twitchgame.game.events.EnterEvent;
import atk.twitchgame.game.events.Event;
import atk.twitchgame.game.events.MovePawnEvent;
import atk.twitchgame.game.gameobjects.Pawn;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.Stage;


import java.util.HashMap;

public class GameWorld {

    private final Stage stage;
    private final HashMap<String, Pawn> pawns = new HashMap<>();
    private final PawnFactory pawnFactory;
    private final TiledMap map;

    public GameWorld(Stage stage, PawnFactory pawnFactory, TiledMap map) {
        this.pawnFactory = pawnFactory;
        this.stage = stage;
        this.map = map;
    }

    public void createPawn(EnterEvent event) {
        Pawn pawn = pawnFactory.createPawn(event.pawnStyle, event.playerNickname);
        stage.addActor(pawn.getActor());
        pawns.put(event.playerNickname, pawn);
    }

    public void walkPawn(MovePawnEvent event) {
        Pawn pawn = pawns.get(event.playerNickname);
        pawn.move(event.direction);
    }

    public void executeEvent(Event event) {
        if(event instanceof EnterEvent) {
            createPawn((EnterEvent) event);
        } else if (event instanceof MovePawnEvent) {
            walkPawn((MovePawnEvent) event);
        }
    }
}
