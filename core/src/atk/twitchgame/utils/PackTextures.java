package atk.twitchgame.utils;


import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class PackTextures {
    public static void main (String[] args) {
        TexturePacker.Settings tpSettings = new TexturePacker.Settings();
        tpSettings.maxWidth = 2048;
        tpSettings.maxHeight = 2048;
        TexturePacker.process(tpSettings, "assets-unpacked", "assets/packed", "base.atlas");
    }
}
