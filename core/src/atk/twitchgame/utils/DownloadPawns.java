package atk.twitchgame.utils;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Do NOT use. Assets are checked-in into the repository.
 */
public class DownloadPawns {
    public static void main(String[] args) throws IOException {
        String outputFolder = "core/assets/pawns/";

        Document doc = Jsoup.connect("http://untamed.wild-refuge.net/rmxpresources.php?characters").get();
        Elements links = doc.select("a[href]");
        for (Element link : links) {
            String target = link.attr("abs:href");
            if(target.endsWith(".png")) {
                System.out.println(target);

                Connection.Response resultImageResponse = Jsoup.connect(target).ignoreContentType(true).execute();
                String name = new File(target).getName();

                FileOutputStream out = (new FileOutputStream(new java.io.File(outputFolder + name)));
                out.write(resultImageResponse.bodyAsBytes());
                out.close();
            }
        }
    }
}
