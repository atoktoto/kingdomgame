package atk.twitchgame.gfx;


import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnimatedTilesParser {

    public static void makeAnimated(TiledMap map) {
        for (TiledMapTileSet tileSet : map.getTileSets()) {
            List<TiledMapTile> animationStarters = new ArrayList<>();
            for(TiledMapTile tile: tileSet){
                Object property = tile.getProperties().get("animation");
                if(property != null) {
                    animationStarters.add(tile);
                }
            }

            for (TiledMapTile animationStarter : animationStarters) {
                int indexStart = animationStarter.getId();
                String listString = (String) animationStarter.getProperties().get("animation");

                List<TiledMapTile> frameList = Stream.of(listString.split(","))
                        .map(Integer::parseInt)
                        .map(id -> tileSet.getTile(id + indexStart))
                        .collect(Collectors.toList());

                Array<StaticTiledMapTile> frames = new Array<>(frameList.size());
                for (TiledMapTile tiledMapTile : frameList) {
                    frames.add((StaticTiledMapTile) tiledMapTile);
                }

                AnimatedTiledMapTile animatedTile = new AnimatedTiledMapTile(1, frames);

                for (MapLayer mapLayer : map.getLayers()) {
                    if(mapLayer instanceof TiledMapTileLayer) {
                        TiledMapTileLayer layer = (TiledMapTileLayer) mapLayer;
                        for(int y = 0; y < layer.getHeight(); y++) {
                            for(int x = 0; x < layer.getWidth(); x++) {
                                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                                if(cell != null && cell.getTile().getId() == animationStarter.getId()) {
                                    cell.setTile(animatedTile);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


}
